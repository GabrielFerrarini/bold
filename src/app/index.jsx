import React from 'react';

import MainContainer from './mainContainer';
import routes from './routes';

const App = () => (
  <div>
    <MainContainer>
      {routes}
    </MainContainer>
  </div>);

export default App;
