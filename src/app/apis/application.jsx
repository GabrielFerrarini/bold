import { acceptJsonHeader, contentJsonHeader } from './headers';
import handler from './resultHandler';

function fetchApplication({ appName }) {
  const headers = contentJsonHeader(acceptJsonHeader());

  return fetch(`${BOLD_APP_API_URL}/applications/${appName}`, {
    method: 'GET',
    headers,
  })
    .then(handler)
    .then(res => res.json())
    .then(data => data);
}

export default {
  fetchApplication,
};
