import React, { Component } from 'react';
import PropTypes from 'prop-types';

import Review from './review';

class ApplicationView extends Component {
  constructor(props) {
    super(props);
    this.props = props;

    this.showMoreItemsClick = this.showMoreItemsClick.bind(this);
    this.searchClick = this.searchClick.bind(this);

    this.appNameInput = React.createRef();

    this.state = {
      start: 0,
      end: 10,
    };
  }

  showMoreItemsClick() {
    this.setState(prevState => ({
      end: prevState.end + 10,
    }));
  }

  searchClick(e) {
    e.preventDefault();
    this.props.onSearch({
      appName: this.appNameInput.current.value,
    });
  }

  render() {
    return (
      <div>
        <div>
          <span>App Name: </span>
          <span>
            <input type="text" ref={this.appNameInput} />
          </span>
          <span>
            <input type="submit" onClick={this.searchClick} value="Search App Reviews" />
          </span>
        </div>
        <div id="appliation-container">
          <ul>
            <li>
              <span>Application: </span>
              <span>{this.props.application ? this.props.application.app_name : null}</span>
            </li>
          </ul>
        </div>
        <div id="appliation-container">
          <div>
            Reviews
          </div>
          <ul>
            {this.props.application ?
              this.props.application.reviews.slice(this.state.start, this.state.end)
                .map(review => (
                  <Review review={review} />
                )) : null}
          </ul>
        </div>
        <div className="more-items" onClick={this.showMoreItemsClick}>
          Show More
        </div>
      </div>
    );
  }
}

ApplicationView.defaultProps = {
  application: {
    app_name: null,
    star_rating: 1,
    reviews: [],
  }, // eslint-disable-line
};

ApplicationView.propTypes = {
  application: PropTypes.object, // eslint-disable-line
  onSearch: PropTypes.func.isRequired,
};

export default ApplicationView;
