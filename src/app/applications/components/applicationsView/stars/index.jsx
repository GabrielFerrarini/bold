import React from 'react';
import PropTypes from 'prop-types';

const Stars = props => (
  <ul>
    <li className="review-star review-star-filled" />
    <li className={`review-star ${props.numberOfStars >= 2 ? 'review-star-filled' : null}`} />
    <li className={`review-star ${props.numberOfStars >= 3 ? 'review-star-filled' : null}`} />
    <li className={`review-star ${props.numberOfStars >= 4 ? 'review-star-filled' : null}`} />
    <li className={`review-star ${props.numberOfStars >= 5 ? 'review-star-filled' : null}`} />
  </ul>
);

Stars.propTypes = {
  numberOfStars: PropTypes.number.isRequired,
};

export default Stars;
