import React from 'react';
import PropTypes from 'prop-types';

import Stars from '../stars';

const Review = props => (
  <li className="review-item">
    <div>
      <ul>
        <li>
          <span className="review-header">Shop Name: </span>
          <span className="review-text">{props.review ? props.review.shop_name : null}</span>
        </li>
        <li>
          <span className="review-header">Review: </span>
          <span className="review-body-text">{props.review ? props.review.body : null}</span>
        </li>
        <li>
          <span className="review-header">Stars: </span>
          <span><Stars numberOfStars={props.review ? props.review.star_rating : null} /></span>
        </li>
      </ul>
    </div>
  </li>
);

Review.propTypes = {
  review: PropTypes.object.isRequired,  // eslint-disable-line
};

export default Review;
