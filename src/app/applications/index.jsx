import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';

import { fetchApplicationRequested } from '../state/applications/actions';
import { getApplicationSelector } from '../state/applications/selectors';

import ApplicationView from './components/applicationsView';

class Application extends Component {
  constructor(props) {
    super(props);
    this.props = props;
  }

  render() {
    return (
      <div className="base-container">
        <ApplicationView
          application={this.props.application}
          onSearch={this.props.fetchApplication}
        />
      </div>
    );
  }
}

Application.defaultProps = {
  application: {
    app_name: null,
    star_rating: 1,
  },
};

Application.propTypes = {
  application: PropTypes.shape({
    app_name: PropTypes.string,
    overall_rating: PropTypes.number,
    reviews: PropTypes.arrayOf(PropTypes.shape({
      author: null,
      body: PropTypes.string,
      created_at: PropTypes.date,
      shop_domain: PropTypes.string,
      shop_name: PropTypes.string,
      star_rating: PropTypes.Number,
    })),
  }),
  fetchApplication: PropTypes.func.isRequired,
};

const mapStateToProps = state => ({
  application: getApplicationSelector(state),
});

const mapDispatchToProps = {
  fetchApplication: fetchApplicationRequested,
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Application));
