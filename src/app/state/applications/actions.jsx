import * as Constants from './constants';

export function fetchApplicationRequested({ appName }) {
  return {
    type: Constants.FETCH_APPLICATION_REQUESTED,
    appName,
  };
}

export function fetchApplicationSuccess(application) {
  return {
    type: Constants.FETCH_APPLICATION_SUCCESS,
    application,
  };
}

export function fetchApplicationError(error) {
  return {
    type: Constants.FETCH_APPLICATION_ERROR,
    error,
  };
}
