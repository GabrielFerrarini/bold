
import { all, takeLatest, call, put } from 'redux-saga/effects';

import applicationApi from '../../apis/application';
import * as Constants from './constants';
import * as Actions from './actions';

function* fetchApplication(action) {
  try {
    const { appName } = action;
    const application = yield call(applicationApi.fetchApplication, { appName });

    yield put(Actions.fetchApplicationSuccess(application));
  } catch (error) {
    yield put(Actions.fetchApplicationError(error));
  }
}

function* watchApplication() {
  yield takeLatest(Constants.FETCH_APPLICATION_REQUESTED, fetchApplication);
}

export default function* watchApplicationSaga() {
  yield all([
    watchApplication(),
  ]);
}
