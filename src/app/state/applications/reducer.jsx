import * as Constants from './constants';

const applicationsInitialState = {
  requesting: false,
  application: null,
  error: null,
};

export default function applicationsReducer(state = applicationsInitialState, action) {
  switch (action.type) {
    case Constants.FETCH_APPLICATION_REQUESTED:
      return {
        ...state,
        requesting: true,
        application: null,
        error: null,
      };
    case Constants.FETCH_APPLICATION_SUCCESS:
      return {
        ...state,
        requesting: false,
        application: action.application,
      };
    case Constants.FETCH_APPLICATION_ERROR:
      return {
        ...state,
        requesting: false,
        error: action.error,
      };
    default:
      return state;
  }
}
