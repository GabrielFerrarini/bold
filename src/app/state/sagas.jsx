import { all } from 'redux-saga/effects';

import watchApplicationSaga from './applications/sagas';

export default function* saga() {
  yield all([
    watchApplicationSaga(),
  ]);
}
