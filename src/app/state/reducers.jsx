import { combineReducers } from 'redux';

import applicationsReducer from './applications/reducer';

export default combineReducers({
  applications: applicationsReducer,
});
