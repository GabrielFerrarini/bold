import React from 'react';
import {
  Router,
  Route,
} from 'react-router-dom';

import Application from './applications';
import history from './history';

export default (
  <Router history={history}>
    <Route path="/" component={Application} exact />
  </Router>
);
