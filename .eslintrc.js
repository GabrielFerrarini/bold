module.exports = {
  extends: "airbnb",
  env: {
    browser: true
  },
  plugins: [
    "react",
    "jsx-a11y",
    "import"
  ],
  rules: {
    'linebreak-style': ["error", "unix"],
    "no-underscore-dangle": ["error", { "allow": ["__REDUX_DEVTOOLS_EXTENSION__"] }],
  },
  "globals": {
    "BOLD_APP_API_URL": true,
  },
};
