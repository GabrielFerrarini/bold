const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const webpack = require('webpack');

const publicPath = path.resolve(__dirname, 'public/');
const bundlePath = path.resolve(__dirname, 'dist/');

// const BOLD_APP_API_URL = 'http://bold-app-review-service:3000';
const BOLD_APP_API_URL = 'http://localhost:3000';

module.exports = {
  mode: 'development',
  entry: ['babel-polyfill', './src/index.jsx'],
  output: {
    filename: 'bundle.js',
    path: bundlePath,
  },
  plugins: [
    new HtmlWebpackPlugin({
      title: 'WitFit',
      template: path.join(publicPath, 'template.html'),
      filename: path.join(bundlePath, 'index.html'),
    }),
    new webpack.DefinePlugin({
      BOLD_APP_API_URL: JSON.stringify(BOLD_APP_API_URL),
    }),
  ],
  module: {
    rules: [{
      test: /\.jsx?$/,
      exclude: /node_modules/,
      use: {
        loader: 'babel-loader',
        options: {
          presets: ['env', 'react'],
        },
      },
    }],
  },
  resolve: {
    extensions: ['*', '.js', '.jsx'],
  },
  devServer: {
    compress: true,
    contentBase: path.join(__dirname, 'public'),
    host: '0.0.0.0',
    inline: true,
    port: 4000,
  },
};
