const mongoose = require('mongoose');

const ApplicationSchema = require('./schemas/applicationSchema');

const Application = mongoose.model('Appications', ApplicationSchema);

module.exports = Application;
