const mongoose = require('mongoose');

const { Schema } = mongoose;

const schemaOptions = require('./schemaOptions');

const ApplicationSchema = new Schema({
  _id: false,
  author: {
    type: String,
  },
  body: {
    type: String,
  },
  created_at: {
    type: Date,
  },
  shop_domain: {
    type: String,
  },
  shop_name: {
    type: String,
  },
  star_rating: {
    type: Number,
  },
}, { ...schemaOptions });

module.exports = ApplicationSchema;
