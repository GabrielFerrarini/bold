const mongoose = require('mongoose');

const { Schema } = mongoose;

const schemaOptions = require('./schemaOptions');
const ReviewSchema = require('./reviewSchema');

const ApplicationSchema = new Schema({
  app_name: {
    type: String,
    unique: true,
    index: true,
  },
  overall_rating: {
    type: Number,
  },
  reviews: [ReviewSchema],
}, { ...schemaOptions, collection: 'Applications' });

ApplicationSchema.query.byEmail = email => (this.find({ email: new RegExp(email, 'i') }));

module.exports = ApplicationSchema;
