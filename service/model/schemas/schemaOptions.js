module.exports = {
  timestamps: true,
  versionKey: false,
  runSetterOnQuery: true,
};
