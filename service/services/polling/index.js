const superagent = require('superagent');
const config = require('config');

const logger = require('../logger');

const pollingInterval = config.get('pollingInterval');
const shopifyAppReviewUrl = config.get('shopifyAppReviewUrl');
const applicationList = config.get('applicationList');
const appNameMacro = '${app-name}';

class Polling {
  constructor(appReviewService) {
    this.runPolling = this.runPolling.bind(this);
    this.fetchAppReview = this.fetchAppReview.bind(this);

    this.appReviewService = appReviewService;
  }

  start(callback) {
    this.runPolling();
    setInterval(this.runPolling, pollingInterval);
    callback();
  }

  async runPolling() {
    try {
      const applications = await Promise.all(
        applicationList.map(async (app_name) => {
          const appReviews = await this.fetchAppReview(app_name)
          const { overall_rating, reviews } = appReviews.body;

          return {
            app_name,
            overall_rating,
            reviews,
          };
        }));

      await this.appReviewService.saveReviews(applications);

      return;
    } catch (error) {
      logger.warn('Error getting list of app reviews', error);
    }
  }

  async fetchAppReview(appName) {
    try {
      const reviewUrl = shopifyAppReviewUrl.replace(appNameMacro, appName);

      const appReview = await superagent
        .get(reviewUrl);

      logger.info('App review successfully retrieved', { appName });

      return appReview;
    } catch (error) {
      logger.warn('Error fetching app', { appName });
    }
  }
}

module.exports = Polling;
