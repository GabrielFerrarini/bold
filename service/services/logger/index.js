const winston = require('winston');
const config = require('config');

const environment = config.get('environment');

const transports = [];

const timestampFormat = () => new Date().toISOString();

if (environment === 'development') {
  transports.push(new winston.transports.Console({
    colorize: true,
    timestamp: timestampFormat,
    level: 'debug',
  }));
}

winston.transports = transports;

const logger = new winston.Logger({
  transports,
});

module.exports = logger;
