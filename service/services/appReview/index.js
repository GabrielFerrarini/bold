const Application = require('../../model/application');

const logger = require('../logger');

class AppReviewService {
  async saveReviews(applications = []) {
    try {
      await Promise.all(applications.map(async application => {
        const appDoc = await Application.findOne({
          app_name: application.app_name,
        });

        if (appDoc) {
          const newApp = {
            overall_rating: application.overall_rating,
            reviews: application.reviews,
          }

          return await Application.updateOne({
            ...newApp,
          });
        };

        const newApp = new Application({
          ...application,
        });

        return await newApp.save();
      }));
      logger.info('Reviews saved');
    } catch (error) {
      logger.warn('Errors saving reviews', error);
    }
  }
}

module.exports = AppReviewService;
