const express = require('express');

const ApplicationController = require('../controllers/applicationController');

const ApplicationRouter = express.Router();

ApplicationRouter.route('/applications/:app')
  .get(ApplicationController.readApplication);

module.exports = ApplicationRouter;
