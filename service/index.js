const App = require('./app');
const logger = require('./services/logger');

App.connect()
  .then(() => {
    App.start();
  })
  .catch((error) => {
    logger.error(`Error starting Express App or connectiong to the database. Reason: ${error.message}`);
    process.exit(1);
  });
  