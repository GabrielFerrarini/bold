const httpStatus = require('http-status-codes');
const Application = require('../model/application');

const ApplicationController = {
  // eslint-disable-next-line
  async readApplication(request, response, next) {
    try {
      const { app: app_name } = request.params;

      const application = await Application.findOne({
        app_name,
      })

      if (!application) {
        return next({
          error: httpStatus.NOT_FOUND,
          message: 'Application not found',
        });
        }

      return response.status(httpStatus.OK).json(application);
    } catch (error) {
      return next({
        error: httpStatus.INTERNAL_SERVER_ERROR,
        message: 'Internal error',
      });
    }
  },
};

module.exports = ApplicationController;
