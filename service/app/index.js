const express = require('express');
const mongoose = require('mongoose');
const config = require('config');
const httpStatus = require('http-status-codes');

const bodyParser = require('body-parser');
const cors = require('cors');

const ApplicationRouter = require('../routes/applicationRoute');
const Polling = require('../services/polling');
const AppReviewService = require('../services/appReview');

const logger = require('../services/logger');

const polling = new Polling(new AppReviewService());

/* eslint-disable no-unused-vars */
function errorHandler(error, request, response, next) {
  let httpError = error;

  if (error.response) {
    httpError = JSON.parse(error.response.text);
  }

  const status = httpError.status || httpError.statusCode || httpStatus.INTERNAL_SERVER_ERROR;
  const message = httpError.message ||
    httpError.description ||
    httpStatus.getStatusText(httpStatus.INTERNAL_SERVER_ERROR);

  return response.status(status).json({
    status,
    message,
  });
}

const app = express();
const corsOptions = {
  origin: config.get('corsWhiteList'),
  optionSuccessStatus: 200,
};

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cors(corsOptions));

app.use(ApplicationRouter);

app.use(errorHandler);

class App {
  static start() {
    app.listen(config.get('port'), () => {
      logger.info(`Server successfully started on port: ${config.get('port')}`);
      polling.start(() => {
        logger.info('Server started polling operation');
      })
    });
  }

  static async connect() {
    mongoose.Promise = global.Promise;

    const host = config.get('mongo.url');
    const port = config.get('mongo.port');
    const database = config.get('mongo.database');
    const authdb = config.get('mongo.authdb');
    const ssl = config.get('mongo.ssl');
    const user = config.has('mongo.user') ? config.get('mongo.user') : null;
    const password = config.has('mongo.password') ? config.get('mongo.password') : null;
    const auth = user && password ? {
      authdb,
      user,
      password,
    } : null;

    let connectionString = `mongodb://${host}:${port}/${database}?ssl=${ssl}`;

    try {
      await mongoose.connect(connectionString, {
        auth,
        connectTimeoutMS: config.get('mongo.timeout'),
      });

      logger.info('Successfully connected to database');
    } catch (error) {
      logger.error(`Error connecting to MongoDB: ${error}`);
      throw error;
    }
  }
}

module.exports = App;
